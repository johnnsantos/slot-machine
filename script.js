// ideia base retirada do site disponibilizado na entrega https://torutsume.net/en/slot-machine-by-javascript/

const botao = document.querySelector('#botao')
const imgs = document.querySelectorAll('#img')

const frutas = [];

for (i = 0; i < 3; i++) {
    frutas[i] =  i + '.svg'
}

const img = document.createElement('img').src = frutas[i]

let contador = 0
let points = 0

document.querySelector("#points").innerHTML = 'Pontuação: ' + points

function roleta() {

    let aleatorio = setInterval(function () {
        contador++
        let esquerda = Math.floor(Math.random() * 3)
        let meio = Math.floor(Math.random() * 3)
        let direita = Math.floor(Math.random() * 3)

        document.esquerda.src = frutas[esquerda]
        document.meio.src = frutas[meio]
        document.direita.src = frutas[direita]

        imgs.forEach(el => el.classList.add('motion'))

        if (contador > 15) {
            let escolhida_esquerda = frutas[esquerda];
            let escolhida_meio = frutas[meio];
            let escolhida_direita = frutas[direita];

            imgs.forEach(el => el.classList.remove('motion'))

            if ((escolhida_esquerda == escolhida_meio) && (escolhida_esquerda == escolhida_direita) && (escolhida_meio == escolhida_direita)) {
                points += 1
                document.getElementById('points').innerHTML = 'Pontuação: ' + points
                document.querySelector(".result").innerHTML = "Parabéns, você ganhou!";

            } else {
                document.querySelector(".result").innerHTML = 'Você perdeu.' + '<br>' + 'Tente de novo!';
            }
            contador = 0;
            clearInterval(aleatorio);
        }
    }, 150)
}

botao.addEventListener('click', () => {
    document.querySelector(".result").innerHTML = ''
    roleta()
})